import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
 return (
    <button className="square" style={props.color} onClick={props.onClick}> 
    {props.value} 
    </button>
  );
}

class Board extends React.Component {  
   renderSquare(i) {
      const isWinner = getWinnerSquares(this.props.squares); 
      
      if(isWinner) {
         if(i === isWinner[0] || i === isWinner[1] || i === isWinner[2]) {
            return <Square key={i} value = {this.props.squares[i]} onClick = {() => this.props.onClick(i)} color = {{backgroundColor: 'yellow'}}/>;
	 }
	 else {
            return <Square key={i} value = {this.props.squares[i]} onClick = {() => this.props.onClick(i)} />;
	 }
      }      
      else {
         return <Square key={i} value = {this.props.squares[i]} onClick = {() => this.props.onClick(i)} />;
      }
}
 
 render() {
    
    let squares = [];
    let boardRow = [];

    for(let i = 0; i < 3; i++)
    {
       for(let j = 0; j < 3; j++)
       {
          squares.push(this.renderSquare(i + j * 3));
       };
       boardRow.push(<div key={i} className="board-row">{squares}</div>);
       squares = [];
    }

    const board = <div>{boardRow}</div>;

    return board;

    }
}

class Game extends React.Component {
 constructor(props) {
     super(props);
     this.state = { 
        history: [{ 
	   squares: Array(9).fill(null),
	}],
        stepNumber: 0,
	xIsNext: true,
     };
 } 

 jumpTo(step) {
    this.setState({
        stepNumber: step,
	xIsNext: (step % 2) === 0,
    });
 }

 handleClick(i) {
     //.slice() is called to copy the squares array of instead mutating the existing array
     const history = this.state.history.slice(0, this.state.stepNumber + 1);
     const current = history[history.length - 1];
     const squares = current.squares.slice(); 
     if (calculateWinner(squares) || squares[i]) { return; }
     squares[i] = this.state.xIsNext ? 'X' : 'O';
     this.setState({
     history: history.concat([{
     squares: squares, }]),
     stepNumber: history.length,
     xIsNext: !this.state.xIsNext,     
     });
 }


 render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
       const desc = move ? 
          'Go to move #' + move : 
	  'Go to game start';
	  if(move === this.state.stepNumber) {
	     return(<li key={move}>
                       <button onClick={() => this.jumpTo(move)}><b><u>{desc}</u></b></button>
	            </li>);
	  }
          else {
            return(<li key={move}>
	              <button onClick={() => this.jumpTo(move)}>{desc}</button>
                  </li>);
	  }
    });
    
    let status;

    if(this.state.stepNumber === 9 && !winner) {
       status = 'Draw!';
    }
    else if (winner) {
       status = 'Winner: ' + winner;
    }
    else {
       status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }
    return (
      <div className="game">
        <div className= "game-board">
	 <Board
	    squares = {current.squares}
	    onClick = {(i) => this.handleClick(i)}
	    />
	</div>
	<div className= "game-info"> 
	 <div> {status}</div>
	 <ol>{moves}</ol>
	</div>
      </div>
    );
  }	
}

ReactDOM.render(
 <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
    const lines = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6],
    ];
    for(let i = 0; i < lines.length; i++) {
         const [a, b, c] = lines[i];
	 if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) { 
	    return squares[a]; 
        }
    }
    return null;
}

function getWinnerSquares(squares) {  
    const lines = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6],
    ];
    for(let i = 0; i < lines.length; i++) {
         const [a, b, c] = lines[i];
	 if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) { 
	    return [a, b, c]; 
        }
    }
    return null;
}

