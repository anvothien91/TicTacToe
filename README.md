This is a small personal project to demonstrate my knowledge of ReactJS/Git.

Link to Tutorial
----------------
https://reactjs.org/tutorial/tutorial.html

I followed the tutorial while attempting some of the additional exercises at the
end of the tutorial.

This was also a opportunity for me to improve my use of Git by making each 
additional feature for this tutorial its own branch.  After confirming that 
each additional feature was implemented successfully, I created a "staging"
branch that pulled each of the remote feature branches and resolved any merge
conflicts that I came across.  Once the staging branch that had all the feature
branches merged was tested, I would then merge staging with master.

This process helps the individual practice the concept of merging and resolving
conflicts without the need to be on a team sharing the same git repo to help 
prepare for when the situation of collaborating on a project *does* come.